/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.mid02;

/**
 *
 * @author Pla
 */
public class MainProgram {

    public static void main(String[] args) {

        Harpy harpy = new Harpy("Harpy", 100, 7, 9, 3, 1, 0);// Create harpy name Harpy
        harpy.info();

        Human human = new Human("CJ", 100, 6, 6, 4, 1, 0);//Create human name CJ
        human.info();

        human.farmingLevel1();
        human.farmingLevel1();
        human.farmingLevel1();
        human.farmingLevel1();
        human.farmingLevel1();
        human.farmingLevel3();
        human.farmingLevel3(5);

        harpy.farmingLevel2(6);
        harpy.farmingLevel3(1);
    }
}
