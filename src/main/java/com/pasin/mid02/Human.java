/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.mid02;

/**
 *
 * @author Pla
 */
public class Human extends Npc {

    public Human(String name, int hp, int mana, int atk, int armor, int level, int exp) {
        super(name, hp, mana, atk, armor, level, exp);
    }

    @Override
    public void info() {// info of Human(Override)
        System.out.println("Hello I am " + name + ">> Hp = " + hp + ", Mana = " + mana + "\n"
                + "                >> Atk = " + atk + ", Armor = " + armor + "\n\n"
                + "        ----- Level = " + level + " Exp = " + exp + "------\n");
    }

}
