/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.mid02;

/**
 *
 * @author Pla
 */
public class Npc {

    protected String name;
    protected int hp;

    protected int mana;
    protected int atk;
    protected int armor;
    protected int level;
    protected int exp;

    public Npc(String name, int hp, int mana, int atk, int armor, int level, int exp) {
        this.name = name;
        this.hp = hp;
        this.mana = mana;
        this.atk = atk;
        this.armor = armor;
        this.level = 1;
        this.exp = 0;
        System.out.println("----------Npc Created----------");
    }

    public void info() {// Show info 
        System.out.println("Hello I am " + name + ">> Hp = " + hp + ", Mana = " + mana + "\n"
                + "                >> Atk = " + atk + ", Armor = " + armor + "\n\n"
                + "        ----- Level = " + level + " Exp = " + exp + "------\n");
    }

    public void levelUp() {// this Method Check Exp If exp >= 100 level+1 and all statushave upgrade
        if (exp >= 100) {
            this.level += 1;
            this.hp += 10;
            this.mana += 1;
            this.atk += 3;
            this.armor += 1;
            this.exp = 0;
        }
    }

    public void farmingLevel1() { // this Method plus exp 25 and use LevelUp() Method
        this.exp += 25;
        levelUp();
        this.info();
    }

    public void farmingLevel2() {
        this.exp += 50;
        levelUp();
        this.info();
    }

    public void farmingLevel3() {
        if (this.level >= 3) {
            this.exp += 100;
            levelUp();
            this.info();
        } else {
            System.out.println("----You Need More Level----");
        }
    }

    public void farmingLevel3(int count3) { //farmingLevel3 by count(Overloading Mehod )

        for (int i = 0; i < count3; i++) {
            this.farmingLevel3();
        }
    }

    public void farmingLevel2(int count2) { //farmingLevel2 by count(Overloading Mehod )

        for (int i = 0; i < count2; i++) {
            this.farmingLevel2();
        }
    }

    public void farmingLevel1(int count1) { //farmingLevel1 by count(Overloading Mehod )
        for (int i = 0; i < count1; i++) {
            this.farmingLevel1();
        }
    }

}
